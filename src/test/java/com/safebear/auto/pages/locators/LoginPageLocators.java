package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

import javax.annotation.sql.DataSourceDefinition;

@Data
public class LoginPageLocators {

    private By usernameLocator = By.id("username");
    private By passwordLocator = By.id("password");
    private By loginButtonLocator = By.id("enter");
    private By loginFailedMessage = By.id("rejectLogin");


}
