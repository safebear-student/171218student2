package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import com.safebear.auto.utils.Utils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {

    LoginPageLocators locators = new LoginPageLocators();
    @NonNull WebDriver driver;

    public String getPageTitle(){

        return driver.getTitle().toString();
            }

            public void enterUsername(String username){
        driver.findElement(locators.getUsernameLocator()).sendKeys(username);
            }

    public void enterPassword(String pwrd) {
        driver.findElement(locators.getPasswordLocator()).sendKeys(pwrd);
    }

        public void clickLoginButton () {
            driver.findElement(locators.getLoginButtonLocator()).click();
        }
    public String findFailedMessage () {
        return driver.findElement(locators.getLoginFailedMessage()).getText();
    }

}
