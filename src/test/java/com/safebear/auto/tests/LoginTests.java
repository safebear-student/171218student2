package com.safebear.auto.tests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {
    @Test
    public void loginTest() {
        //Step 1 ACTION: Open our web application in the browser
        driver.get(Utils.getUrl());

        //Step 1 EXPECTED RESULT: check were on login page
        Assert.assertEquals(loginPage.getPageTitle(), "Login Page",
                "The Login Page didnt open, or the title text has changed");
        //Step 2: ACTION: Enter Username and Password
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");

        //Step 3: ACTION: Press the login button
        loginPage.clickLoginButton();
        //step 3: EXPECTED RESULT: check that we're on the Tools Page
        Assert.assertEquals(toolsPage.getPageTitle(), "Tools Page",
                "The Login Page didnt open, or the title text has changed");
        //step 3: EXPECTED RESULT: success message is shown
        Assert.assertEquals(toolsPage.findSuccessMessage(), "Login Successful", "The log in was succesful");
    }


       @Test
       public void failedLogin(){
            //Step 1 ACTION: Open our web application in the browser
            driver.get(Utils.getUrl());

            //Step 1 EXPECTED RESULT: check were on login page
            Assert.assertEquals(loginPage.getPageTitle(), "Login Page" ,
                    "The Login Page didnt open, or the title text has changed");

            //Step 2: ACTION: Enter Username and Password
            loginPage.enterUsername("tester");
            loginPage.enterPassword("letmeinplease");
            //Step 3: ACTION: Press the login button
            loginPage.clickLoginButton();
            //step 3: EXPECTED RESULT: check for error message
            Assert.assertEquals(loginPage.findFailedMessage(), "WARNING: Username or Password is incorrect", "The login was not successful");
        }


    }

